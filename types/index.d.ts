export interface LogstashHTTPAppender extends Appender {
  type: "log4js-logstash-http-json";
  url: string;
  timeout?: number; //defaults to 5000
  application?: string;
  logChannel?: string;
  logType?: string;
}
